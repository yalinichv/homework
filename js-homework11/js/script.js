/*Почему для работы с input не рекомендуется использовать события клавиатуры?
* Возможно из за того что, если у пользователю будет планшет или смартфон события клавиатуры сробатывать не будут*/

const buttons = document.querySelectorAll(".btn");
let lastButton = null;

document.addEventListener("keydown", (event) => {


    if (lastButton !== null && lastButton.classList.contains("active")) {
        lastButton.classList.remove("active");
        lastButton.classList.add("inactive");
    }

    for (let btn of buttons) {

        if (event.key.toUpperCase() === btn.innerText.toUpperCase()) {

            if (btn.classList.contains("inactive")) {
                btn.classList.remove("inactive");
                btn.classList.add("active");
            }
            lastButton = btn;
            break;
        }

    }
});