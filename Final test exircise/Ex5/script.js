function isSpam(string, spamWord, reapedSpamWord) {
    let count = null;
    let words = string.split(' ');

    for (let word of words) {

        if (word === spamWord) {
            count++;

            if (count > reapedSpamWord) {
                return true;
            }
        }
    }
    return false;
}

let comment = document.getElementById("comment");
let spamWord = document.getElementById("spam-word");
const answer = document.getElementById("spam-check-result");

const submit = document.getElementById('send-comment');

submit.addEventListener("click", () => {

    if (isSpam(comment.value, spamWord.value, 3))
        answer.innerText = "Спам есть";
    else
        answer.innerText = "Спама нет";


});