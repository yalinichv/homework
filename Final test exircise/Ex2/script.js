let creditCard = {
    money: 10000,
    pinCode: '1234',
    try: 3,
    status: "active",

    getCash: (pinCode, money) => {
        money = parseInt(money);

        if (creditCard.status === "disabled") {
            return  'Ваша карта заблокирована, обратитесь в банк для ее разблокировки';
        } else if (pinCode !== creditCard.pinCode) {
            creditCard.try = creditCard.try - 1;
            if (creditCard.try > 0) {
                return "Неправильный пин-код! Попробуйте пожалуйста снова!";
            } else {
                creditCard.status = "disabled";
                return 'Неправильный пин-код! Вы исчерпали количество попыток. Ваша карта заблокирована, обратитесь в банк для ее разблокировки';
            }
        } else if (creditCard.money >= money) {
            creditCard.money -= money;
            creditCard.try = 3;
            return `Получите ваши ${money} $`;
        } else {
            creditCard.try = 3;
            return `К сожалению, на вашем счету недостаточно средств`;
        }
    }
};

let pinCode = document.getElementById("pin-code");
let money = document.getElementById("money-sum");
const answer = document.getElementById("cash-request-result");

const submit = document.getElementById('get-cash');

submit.addEventListener("click", () => {

    answer.innerText = creditCard.getCash(pinCode.value, money.value);
});