$(document).ready(function () {

    const tabs = $('.tabs-title');
    const tabsContent = $('.tabs-content-li');

    hideTabs();

    tabs.filter(':first').addClass('active');
    tabsContent.filter(':first').show();


    function clickOnTabMenu(selectTab) {

        tabsContent.each(function (index, element) {

            if ($(element).attr('data-name') === $(selectTab).attr("data-name")) {

                hideTabs();
                tabs.removeClass('active');

                $(element).show();
                $(selectTab).addClass('active');
            }
        });
    }

    function hideTabs() {
        tabsContent.each(function (index, element) {
            tabsContent.hide();
        });
    }

    tabs.on('click', (e) => {
        clickOnTabMenu(e.target);
    });

});

