const cssVariable = document.querySelector("body");
const checkBox = document.querySelector(".switch_3");
const toggleSwitch = document.querySelector(".toggle_switch");
let check = null;

toggleSwitch.addEventListener("click", ()=>{
    checked();
});

function makeInitialState() {
    cssVariable.style.setProperty("--color2", localStorage.getItem("color"));
    check = localStorage.getItem("state");

    if (check === 'true')
    checkBox.checked= true;
}

function checked() {
    if (checkBox.checked) {
        localStorage.setItem(`color`, `#f2f2f2`);
        localStorage.setItem(`state`, `true`);
    cssVariable.style.setProperty("--color2", localStorage.getItem("color"));


    } else {
        localStorage.setItem(`color`, `#171a1c`);
        localStorage.setItem(`state`, `false`);
        cssVariable.style.setProperty("--color2", localStorage.getItem("color"));

    }
}

makeInitialState();