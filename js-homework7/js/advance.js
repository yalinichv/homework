function createList(array) {
    const elementUl = document.createElement("ul");

    document.body.prepend(elementUl);

    let elementsLi = array.map(item => {
        let nestedUl = "";

        if (!(typeof (item) === 'object')) {
            return `<li>${item}</li>`;
        } else {
            if (Array.isArray(item)) {
                for (let nestedItem of item) {
                    nestedUl += `<li>${nestedItem}</li>`;
                }
            } else {
                for (let nestedItem in item) {
                    nestedUl += `<li>${item[nestedItem]}</li>`;
                }
            }

            return `<ul>${nestedUl}</ul>`
        }

    });

    for (let li of elementsLi) {
        elementUl.innerHTML += li;
    }

}

createList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', ["NY", "LA", "DC", "CG"], {name: "dwadwad"}]);
// createList(['1', '2', '3', 'sea', 'user', 23]);
