
/*DOM – это представление HTML-документа в виде дерева тегов.*/


function createList(array) {
    const elementUl = document.createElement("ul");

    document.body.prepend(elementUl);


    let elementsLi = array.map(item =>{
        return `<li>${item}</li>`;
    });

    for (let li of elementsLi){
        elementUl.innerHTML += li;
    }

}

createList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', ["NY", "LA", "DC", "CG"]]);
createList(['1', '2', '3', 'sea', 'user', 23]);
