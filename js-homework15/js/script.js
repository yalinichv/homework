/// <reference path = "../js/jquery/jquery.js" />

$(document).ready(function () {


    $('a[href^="#"]').on('click', (event) => {
        event.preventDefault();

        const id = $(event.target).attr('href'),
            top = $(id).offset().top;
        $('html').animate();

    });

    const button = $('.scroll-btn');

    button.on('click',()=>{
        $('html').animate({ scrollTop: top }, 1000);
    });


    $(window).scroll(() => {
        const winHeight = $(window).innerHeight();
        const setTop = $(window).scrollTop();

        if (setTop > winHeight) {
            button.fadeIn()
        } else { button.fadeOut() }
    });


});