
function Hamburger(size, ingredient) {

    if(!size || !size.size){
        throw new HamburgerException("Has been entered wrong argument");
    }

    if(!ingredient || ingredient.topping){
        throw new HamburgerException("Has been entered wrong argument");
    }

    this.size = size;
    this.ingredient = ingredient;

    this.ingredients = [];
    this.ingredients.push(ingredient);


}


Hamburger.SIZE_SMALL = { name: "small", price: 50, kcal: 20, size: true };
Hamburger.SIZE_LARGE = { name: "large", price: 100, kcal: 40, size: true };
Hamburger.STUFFING_CHEESE = { name: "cheese", price: 10, kcal: 20, ingredient: true, topping: false };
Hamburger.STUFFING_SALAD = { name: "salad", price: 20, kcal: 5, ingredient: true, topping: false };
Hamburger.STUFFING_POTATO = { name: "potato", price: 15, kcal: 10, ingredient: true, topping: false };
Hamburger.TOPPING_MAYO = { name: "mayo", price: 20, kcal: 5, ingredient: true, topping: true };
Hamburger.TOPPING_SPICE = { name: "spice", price: 15, kcal: 0, ingredient: true, topping: true };




Hamburger.prototype.addTopping = function (element) {

    if (!(element && element.ingredient && element.topping)) {
        throw new HamburgerException("Has been entered wrong argument");
    }
    if (this.ingredients.length <= 0) {
        ingredients.push(element);
    }
    else {
        let temp = null;
        this.ingredients.forEach(function (collectItem) {
            if (collectItem.name !== element.name) {
                temp = element;
            }
            else {
                throw new HamburgerException("This " + element.name + " has been already exist");
            }
        }.bind(this));
        this.ingredients.push(temp);
    }

};


Hamburger.prototype.removeTopping = function (element) {

    if (!(element && element.ingredient && element.topping)) {
        throw new HamburgerException("Has been entered wrong argument");
    }

    if (this.ingredients.length === 0) {
        throw new HamburgerException("Collectoins is empty");
    }
    else {
        let flagDel = false;
        this.ingredients.forEach(function (collectItem, index) {
            if (collectItem.name === element.name) {
                this.ingredients.splice(index, 1);
                flagDel = true;
            }

        }.bind(this));

        if (flagDel) {
            flagDel = false;
        }
        else {
            throw new HamburgerException("This " + element.name + " hasnt been exist");
        }
    }
};


Hamburger.prototype.getToppings = function () {

    if (this.ingredients.length === 0) {
        throw new HamburgerException("Have no topping");
    }
    else {
        let tempArr = [];
        this.ingredients.forEach(function (collectItem) {
            if (collectItem.topping) {
                tempArr.push(collectItem);

            }

        }.bind(this));
        return tempArr;
    }

};


Hamburger.prototype.getSize = function () {

    return this.size;

};


Hamburger.prototype.getStuffing = function () {

    if (this.ingredients.length === 0) {
        throw new HamburgerException("Have no stuffing");
    }
    else {
        let tempArr = [];
        this.ingredients.forEach(function (collectItem) {
            if (!collectItem.topping) {
                tempArr.push(collectItem);

            }

        }.bind(this));
        return tempArr;
    }

};


Hamburger.prototype.calculatePrice = function () {
    let sum = null;
    this.ingredients.forEach(function (element) {
        sum += element.price;
    }.bind(this));
    return sum + this.size.price;
};


Hamburger.prototype.calculateCalories = function () {
    let sum = null;
    this.ingredients.forEach(function (element) {
        sum += element.kcal;
    }.bind(this));
    return sum + this.size.kcal;
};


function HamburgerException(message, name) {
    this.message = message;
    this.name = name;
}


try {
    // маленький гамбургер с начинкой из сыра
    var hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);
    // добавка из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    // спросим сколько там калорий
    console.log("Calories: %f", hamburger.calculateCalories());
    // сколько стоит
    console.log("Price: %f", hamburger.calculatePrice());
    // я тут передумал и решил добавить еще приправу
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    // А сколько теперь стоит? 
    console.log("Price with sauce: %f", hamburger.calculatePrice());
    // Проверить, большой ли гамбургер? 
    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
    // Убрать добавку
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Have %d toppings", hamburger.getToppings().length); // 1

} catch (error) {

    console.log(error.message);

}


