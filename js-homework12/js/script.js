/*setTimeout Это метод встроенный в браузер, который принемает два параметра 1) callBack function, 2) delay - время через которое
* выполница функция коллБэк, выполнится она один раз*/
/*setInterval Это тоже встроенынй метод и принемает теже параметры что и setTimeout, но выполняется бесконечное колиство раз,
если его не остановить, остановить их можно спомощью функций clearInterval() и clearTimeout()*/


const buttonStop = document.createElement("button");
buttonStop.innerText = "stop";
document.body.prepend(buttonStop);

const buttonStart = document.createElement("button");
buttonStart.innerText = "start";
document.body.prepend(buttonStart);

const timerText = document.createElement("span");
container.appendChild(timerText);

const images = document.querySelectorAll("img");

let delay = 10000;
let timerId = null;
let timerInterval = null;
let lastImg = null;
let count = 0;
let currentSec = null;
let currentMs = null;


showImages();

function showImages() {

    if (count === images.length) {
        count = 0;
    }

    if (count <= images.length) {
        if (images[count].classList.contains("hide")) {
            images[count].classList.add("show");
            images[count].classList.remove("hide");
        }

        if (lastImg !== null && lastImg.classList.contains("show")) {
            lastImg.classList.add("hide");
            lastImg.classList.remove("show");
        }
        lastImg = images[count];
        count++;
    }
    timer(delay);
    timerId = setTimeout(showImages, delay);

}

function timer(delay) {
    const timerCurrent = delay;
    const timeNow = new Date();
    const timeEnd = new Date(Date.parse(timeNow.toString()) + timerCurrent);

    timerInterval = setInterval(() => {
        const timeNow = new Date();
        const differenceTime = timeEnd.getTime() - timeNow.getTime();

        if (differenceTime < 0) {
            clearInterval(timerInterval);
            return;
        }
        currentSec = Math.floor((differenceTime / 1000) % 60);
        currentMs = (differenceTime - currentSec * 1000);

        timerText.innerText = `Timer: ${currentSec}:${currentMs.toString()}`;
    }, 1)
}

buttonStop.addEventListener("click", () => {
    clearTimeout(timerId);
    clearInterval(timerInterval);
    timerInterval = null;
    timerId = null;
});

buttonStart.addEventListener("click", () => {

    if (timerId === null&& timerInterval===null) {
        showImages(count);
    }


});
