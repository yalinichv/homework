$(document).ready(function () {
    /**
     * This is function makes tabs work.
     */


    function makingTabs() {


        const tabs = $('.services-tab');
        tabs.filter(':first').addClass('services-tab-active');

        const tabsContent = $('.services-tab-content');
        tabsContent.filter(':first').addClass('services-tab-content-active');

        const tabsContainer = $('our-services-tabs');

        function clickOnTabMenu(tab) {

            tabsContent.each(function (index, element) {


                if ($(element).attr('data-set') === $(tab).attr("data-set")) {
                    tabsContent.removeClass('services-tab-content-active');
                    tabs.removeClass('services-tab-active');

                    $(element).addClass('services-tab-content-active');
                    $(tab).addClass('services-tab-active');
                }
            });
        }


        tabs.on('click', (e) => {
            clickOnTabMenu(e.target);
        });
    }
    makingTabs();

    /**
     * This is function makes load more pictures and sort pictures at category.
     */
    function loadMorePictures() {

        const btnLoadMore = $('.amazing-button');
        const gridItems = $('.amazing-item');
        let pushCount = 2;
        let countPictures = 24;

        const tabs = $('.ready-tab');

        showFirstItem();

        function sortItems(event) {

            const gridItems = $('.amazing-item');

            let visibleItems = $(gridItems).map((index, element) => {

                if ($(element).hasClass('amazing-item-active')) {

                    return element;
                }
            });
            
            if ($(event.target).attr('data-set') === 'all') {
                $(tabs).removeClass('ready-tab-active');
                $(event.target).addClass('ready-tab-active');
                
                $(visibleItems).show("slow");
            } else {
                visibleItems.each((index, element) => {
                    if ($(element).attr('data-set') === $(event.target).attr('data-set')) {
                        $(tabs).removeClass('ready-tab-active');
                        $(event.target).addClass('ready-tab-active');

                        $(element).show("slow");
                    } else {
                        $(tabs).removeClass('ready-tab-active');
                        $(event.target).addClass('ready-tab-active');

                        $(element).hide("slow");
                    }
                });
            }
        }

        function showFirstItem() {


            gridItems.each((index, element) => {

                if (index < 12) {
                    $(element).show();
                    $(element).addClass("amazing-item-active");
                }
            });
        }

        tabs.on('click', sortItems);

        btnLoadMore.on('click', () => {

            if (pushCount !== 0) {

                gridItems.each((index, element) => {
                    if (index < countPictures) {
                        $(element).show(500);
                        $(element).addClass("amazing-item-active");

                    }
                });

                $(tabs).removeClass('ready-tab-active');
                $('.ready-tab:first').addClass('ready-tab-active');

                countPictures += 12;
                --pushCount;

                if (pushCount === 0) {
                    btnLoadMore.hide(400);
                }

            }

        });

    }
    loadMorePictures();

    /**
     * This is functions connects module Slick.
     */
    function slick() {
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.slider-nav',
            adaptiveHeight: false,
            speed:700
        });
        $('.slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            dots: false,
            arrows: true,
            focusOnSelect: true,
            speed:500,
            accessibility: true,
            autoplay:true,
            adaptiveHeight:true
        });

    }
    slick();


});