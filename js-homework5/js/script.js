/* Экронирование необходимо для того чтобы интерпретатор понимал что это не спец символ, а просто строка */

function createNewUser() {
    let firstNameValue = prompt("Please, enter your name:");
    let lastNameValue = prompt("Please, enter your last name:");
    let birthdayValue = prompt("Please, enter your birth date dd.mm.yyyy:");

    let birthdayDate = convertToDate();

    function convertToDate() {

        let arr = birthdayValue.split('.');
        arr.reverse();
        const str = arr.join('-');
        return new Date(str);
    }

    const newUser = {

        get firstName() {
            return firstNameValue
        },
        set firstName(newValue) {
            firstNameValue = newValue;
        },


        get lastName() {
            return lastNameValue
        },
        set lastName(newValue) {
            lastNameValue = newValue;
        },

        get birthday() {
            return birthdayDate;
        },

        getLogin() {
            let userName = '';
            userName = this.firstName.charAt(0) + this.lastName;
            return userName.toLowerCase();
        },

        getAge() {
            debugger;

            let todayYear = new Date().getFullYear();
            return todayYear - birthdayDate.getFullYear();
        },

        getPassword() {
            let password = '';
            password = this.firstName.charAt(0).toUpperCase() +
                this.lastName.toLowerCase() +
                this.birthday.getFullYear();
            return password;
        }
    };

    return newUser;
};

let user = createNewUser();

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
