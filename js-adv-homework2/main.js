
class Hamburger {

    constructor(size, ingredient) {

        if (!size || size.size && !ingredient || ingredient.topping) {
            throw new HamburgerException("Has been entered wrong argument");
        }

        this._size = size;
        this._ingredient = ingredient;

        this._ingredients = [];
        this._ingredients.push(this._ingredient);
    }

    get size() {
        return this._size;
    }

    static SIZE_SMALL = { name: "small", price: 50, kcal: 20, size: true };
    static SIZE_LARGE = { name: "large", price: 100, kcal: 40, size: true };
    static STUFFING_CHEESE = { name: "cheese", price: 10, kcal: 20, ingredient: true, topping: false };
    static STUFFING_SALAD = { name: "salad", price: 20, kcal: 5, ingredient: true, topping: false };
    static STUFFING_POTATO = { name: "potato", price: 15, kcal: 10, ingredient: true, topping: false };
    static TOPPING_MAYO = { name: "mayo", price: 20, kcal: 5, ingredient: true, topping: true };
    static TOPPING_SPICE = { name: "spice", price: 15, kcal: 0, ingredient: true, topping: true };

    addTopping(element) {
        if (!(element && element.ingredient && element.topping)) {
            throw new HamburgerException("Has been entered wrong argument");
        }
        if (this._ingredients.length <= 0) {
            this._ingredients.push(element);
        }
        else {
            let temp = null;
            this._ingredients.forEach(function (collectItem) {
                if (collectItem.name !== element.name) {
                    temp = element;
                }
                else {
                    throw new HamburgerException(`This ${element.name} has been already exist`);
                }
            }.bind(this));
            this._ingredients.push(temp);
        }
    };

    removeTopping(element) {
        if (!(element && element.ingredient && element.topping)) {
            throw new HamburgerException("Has been entered wrong argument");
        }

        if (this._ingredients.length === 0) {
            throw new HamburgerException("Collections is empty");
        }
        else {
            let flagDel = false;
            this._ingredients.forEach(function (collectItem, index) {
                if (collectItem.name === element.name) {
                    this._ingredients.splice(index, 1);
                    flagDel = true;
                }

            }.bind(this));

            if (flagDel) {
                flagDel = false;
            }
            else {
                throw new HamburgerException(`This ${element.name} has not been exist`);
            }
        }
    };

    getToppings() {
        if (this._ingredients.length === 0) {
            throw new HamburgerException("Have no topping");
        }
        else {
            let tempArr = [];
            this._ingredients.forEach(function (collectItem) {
                if (collectItem.topping) {
                    tempArr.push(collectItem);
                }
            }.bind(this));
            return tempArr;
        }
    };

    getSize() {
        return this.size;
    };

    getStuffing() {

        if (this._ingredients.length === 0) {
            throw new HamburgerException("Have no stuffing");
        }
        else {
            let tempArr = [];
            this._ingredients.forEach(function (collectItem) {
                if (!collectItem.topping) {
                    tempArr.push(collectItem);

                }

            }.bind(this));
            return tempArr;
        }

    };


    calculatePrice() {
        let sum = null;
        this._ingredients.forEach(function (element) {
            sum += element.price;
        }.bind(this));
        return sum + this.size.price;
    };


    calculateCalories() {
        let sum = null;
        this._ingredients.forEach(function (element) {
            sum += element.kcal;
        }.bind(this));
        return sum + this.size.kcal;
    };
}

class HamburgerException {

    constructor(message, name) {
        this.message = message;
        this.name = name;
    }

}


try {
    // маленький гамбургер с начинкой из сыра
    let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);
    // добавка из майонеза
    hamburger.addTopping(Hamburger.TOPPING_MAYO);
    // спросим сколько там калорий
    console.log("Calories: %f", hamburger.calculateCalories());
    // сколько стоит
    console.log("Price: %f", hamburger.calculatePrice());
    // я тут передумал и решил добавить еще приправу
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    // А сколько теперь стоит?
    console.log("Price with sauce: %f", hamburger.calculatePrice());
    // Проверить, большой ли гамбургер?
    console.log("Is hamburger large: %s", hamburger.getSize() === Hamburger.SIZE_LARGE); // -> false
    // Убрать добавку
    hamburger.removeTopping(Hamburger.TOPPING_SPICE);
    console.log("Have %d toppings", hamburger.getToppings().length); // 1

} catch (error) {

    console.log(error.message);

}


