/*Циклы нужны для многократного выполнения однотипного кода, чтобы не дублировать его*/

let number1 = +prompt("Enter first number:");

while (!((number1 ^ 0) === number1)) {
    number1 = +prompt("Enter first number, again:");
}
let number2 = +prompt("Enter second number:");

while (!((number2 ^ 0) === number2)) {
    number2 = +prompt("Enter second number, again:");
}

if (number2 > 4) {

    for (let i = number1; i < number2; i++) {
        if (i % 5 === 0) {
            console.log(i);
        }
    }
} else {
    console.log(`Sorry, no number`);
}

