/*forEach() Это метод массива, который перебирает каждый элемент массива и выполняет указанную функцию один раз для каждого эелемента в массиве.*/


function filterBy(processArray, withoutType) {

let backArray = [];

for (let item of processArray) {

    if (typeof item !== typeof withoutType) {
        backArray.push(item);
    }
}
return backArray;
}

const a = ['hello', 'world', 23, '23', null];

console.log(filterBy(a,""));