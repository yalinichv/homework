
const btn = document.querySelector(".nav-btn");
const menu = document.querySelector(".menu");

btn.addEventListener("click", () => {
    menu.classList.toggle("active");
    btn.classList.toggle("nav-btn");
    btn.classList.toggle("show");

})