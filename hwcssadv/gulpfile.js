const gulp = require('gulp');
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const cleanCss = require('gulp-clean-css');
const autoprefix = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const jsMinify = require('gulp-js-minify');
const browSync = require('browser-sync').create();

const cleanDir = () => {
    return gulp.src('dist/**/', { read: false })
        .pipe(clean());

}

const buildCss = () => {
    return gulp.src('src/scss/globalstyle.scss')
        .pipe(sass())
        .pipe(autoprefix({
            cascade: false
        }))
        .pipe(cleanCss({ compatibility: 'ie8' }))
        .pipe(gulp.dest('dist/css/'));
}

const jsBuild = () => {
    return gulp.src('src/js/main.js')
        .pipe(jsMinify())
        .pipe(uglify())
        .pipe(gulp.dest('dist/js/'));
};

const imagesBuild = () => {
    return gulp.src('src/img/**/*.*')
        .pipe(gulp.dest('dist/img/'));
};


const watch = () => {
    gulp.watch('src/scss/**/*.*', buildCss).on('change', browSync.reload);
    gulp.watch('*.html').on('change', browSync.reload);
    gulp.watch('src/js/*.js', jsBuild).on('change', browSync.reload);
    browSync.init({
        server: {
            baseDir: "./"
        }
    });
};

gulp.task('dev', watch);
gulp.task('build', gulp.series(cleanDir, buildCss, jsBuild, imagesBuild));


