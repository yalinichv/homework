const inputFirst = document.querySelector("#pass-first");
const inputSecond = document.querySelector("#pass-second");
const iElements = document.querySelectorAll("i");
const button = document.querySelector(".btn");
const errorMes = document.createElement("span");

function switchClass(element, input) {

    if (element.classList.contains("fa-eye-slash")) {
        element.classList.remove("fa-eye-slash");
        element.classList.add("fa-eye");

        input.setAttribute("type", "text");
    } else {
        element.classList.remove("fa-eye");
        element.classList.add("fa-eye-slash");

        input.setAttribute("type", "password");
    }
}

function equalInputValue() {
    if (inputFirst.value === inputSecond.value && inputFirst.value !== ""
                                               && inputSecond.value !=="") {
        errorMes.remove();
        alert("You are welcome");

    } else {
        errorMes.innerText = "Нужно ввести одинаковые значени";
        errorMes.classList.add("error");
        document.querySelector("#secondLabel").appendChild(errorMes);
    }
}

iElements[0].addEventListener("click", () => {
    switchClass(iElements[0], inputFirst);
});

iElements[1].addEventListener("click", () => {
    switchClass(iElements[1], inputSecond);
});

button.addEventListener("click", equalInputValue);

