

let tabsContainer = document.querySelector(".tabs");
let tabs = document.getElementsByClassName("tabs-title");
let tabsContent = document.querySelectorAll(".tabs-content-li");
let previousEvent = null;

/*Фуниция которая удаляет таб по нажатию на крестик*/
function delTab(event) {

    if (event.target.childNodes.length > 1) {
        return null;
    } else {
        if (tabs.length > 1) {
            let del = document.createElement("span");
            del.classList.add("del");
            del.innerText = "X";
            event.target.appendChild(del);

            del.addEventListener("click", () => {
                event.target.remove();
            });
        } else {
            return null;
        }
        return event;
    }

};

/*Фуниция активирует таб*/
function activeTab(event) {
    event.target.classList.add("active");
    previousEvent = delTab(event);
};

/*Фуниция деактивирует таб*/
function inActiveTab(event) {
    event.target.childNodes[1].remove();
    event.target.classList.remove("active")
}

/*Фуниция показывает содержимое таба*/
function showContent(event) {

    tabsContent.forEach(item => {
        let tabsItem = item.getAttribute('data-name');
        let contentTabItem = event.target.getAttribute('data-name');
        if (tabsItem === contentTabItem) {
            item.classList.remove("hidden");
        }
        else{
            item.classList.add("hidden");
        }
    })
}

/*Фуниция-обработчик*/
function handler(event) {

    if (previousEvent !== null) {
        inActiveTab(previousEvent);

    }
    activeTab(event);
    showContent(event);
};

/*Слушатель событий*/
tabsContainer.addEventListener("click", handler);