/*Событие это порождение какого-то нового действия, в случаи JS при взаимодействия
* с каким либо элементом на странице, (будь то клиз или наведенее итд) на элемент, вернет
* результат выполнения функции =)*/

const form = document.createElement("form");
document.body.prepend(form);

const container = document.createElement("div");
container.classList.add("container");
form.prepend(container);


const spanCurrentPrice = document.createElement("span");
spanCurrentPrice.classList.add("currentPrice");


const spanTitle = document.createElement("span");
spanTitle.classList.add("priceTitle");
spanTitle.innerText = "Price: ";

container.appendChild(spanTitle);


const inputText = document.createElement("input");
inputText.classList.add("inputPrice");

container.appendChild(inputText);


const spanPriceIncorrect = document.createElement("span");
spanPriceIncorrect.classList.add("incorrectPrice");
spanPriceIncorrect.innerText = "Please enter correct price";


function focusOn() {
    inputText.classList.remove("borderDefault");
    inputText.classList.remove("borderRed");
    inputText.classList.add("borderGreen");
    spanPriceIncorrect.remove();

};

function blurOn() {
    if (+inputText.value < 0 || Number.isNaN(+inputText.value)) {
        inputText.classList.remove("borderDefault");
        inputText.classList.remove("borderGreen");
        inputText.classList.add("borderRed");
        container.appendChild(spanPriceIncorrect);
    } else {
        inputText.classList.remove("borderGreen");
        inputText.classList.add("borderDefault");

        if (inputText.value.trim() > 0) {
            container.appendChild(spanCurrentPrice);
            spanCurrentPrice.innerText = `Current price: ${inputText.value}`;

            const del = document.createElement("span");

            del.classList.add("del");
            del.innerText = "x";
            spanCurrentPrice.appendChild(del);
            del.addEventListener("click", () => {
                spanCurrentPrice.remove();
            })
        }
    }

};


inputText.addEventListener("focus", focusOn);
inputText.addEventListener("blur", blurOn);