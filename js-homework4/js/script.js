/*Метод объекта - это функция которая описывает какое-то поведение объекта, его действие.*/

function createNewUser() {
    let firstNameValue = prompt("Please, enter your name:");
    let lastNameValue = prompt("Please, enter your last name:");


    const newUser = {

        get firstName() {
            return firstNameValue
        },
        set firstName(newValue) {
            firstNameValue = newValue;
        },


        get lastName() {
            return lastNameValue
        },
        set lastName(newValue) {
            lastNameValue = newValue;
        },


        getLogin() {
            let userName = '';
            userName = this.firstName.charAt(0) + this.lastName;
            return userName.toLowerCase();
        }
    };


    return newUser;
};

let user = createNewUser();

console.log(user.getLogin());


